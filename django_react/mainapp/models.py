from django.db import models
from django.urls import reverse


class Course(models.Model):
    name = models.CharField("Название курса", max_length=30)
    description = models.CharField("Описание курса", max_length=255, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('course-update', kwargs={'pk': self.pk})


class Student(models.Model):
    first_name = models.CharField("Имя студента", max_length=30)
    second_name = models.CharField("Фамилия студента", max_length=30)
    last_name = models.CharField("Отчество студента", max_length=30)
    rating = models.DecimalField("Рейтинг студента", decimal_places=1, max_digits=5)
    courses = models.ManyToManyField(Course)

    def __str__(self):
        return f"{self.first_name} {self.second_name} {self.last_name}"
