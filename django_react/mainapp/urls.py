from django.urls import path, re_path, include
from rest_framework.routers import DefaultRouter

from mainapp import views
from mainapp.views import CourseViewSet, StudentViewSet

router = DefaultRouter()
router.register("courses", CourseViewSet)
router.register("students", StudentViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.index),
    path('course/', views.index),
    re_path(r'^course/\d+/', views.index),
]