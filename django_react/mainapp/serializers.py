from rest_framework import serializers

from mainapp.models import Course, Student


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = "pk", "name", "description"
        view_name = "courses"


class StudentSerializer(serializers.ModelSerializer):
    courses = CourseSerializer(many=True, required=False)

    class Meta:
        model = Student
        fields = "pk", "first_name", "second_name", "last_name", "courses"
        view_name = "students"
