import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class CoursesService{

    constructor(){}


    getCourses() {
        const url = `${API_URL}/api/courses/`;
        return axios.get(url).then(response => response.data);
    }
    getCourse(pk) {
        const url = `${API_URL}/api/courses/${pk}/`;
        return axios.get(url).then(response => response.data);
    }
    createCourse(course){
        const url = `${API_URL}/api/courses/`;
        return axios.post(url,course);
    }
    updateCourse(course){
        const url = `${API_URL}/api/courses/${course.pk}/`;
        return axios.put(url,course);
    }
    deleteCourse(course){
        const url = `${API_URL}/api/courses/${course.pk}`;
        return axios.delete(url);
    }
}