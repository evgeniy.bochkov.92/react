import { render } from "react-dom";
import React, { Component } from "react";
import axios from 'axios'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      courses: [],
    };
  }

  componentDidMount() {
    axios.get('/api/courses')
      .then(response => {
        const courses = response.data;
        this.setState({ courses });
      });
  }

  render() {
    return (
      <ul>
        {this.state.courses.map(course => {
          return (
            <li key={course.id}>
              {course.name}
            </li>
          );
        })}
      </ul>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);