import React, { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Route, Link } from 'react-router-dom'

import CoursesList from './CoursesList'
import CourseCreateUpdate from './CourseCreateUpdate'
import './App.css';

const BaseLayout = () => (
<div className="container-fluid">
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
                <a className="nav-item nav-link" href="/">COURSES</a>
                <a className="nav-item nav-link" href="/course">CREATE COURSE</a>
            </div>
        </div>
    </nav>
    <div className="content">
        <Route path="/" exact component={CoursesList} />
        <Route path="/course/:pk" component={CourseCreateUpdate} />
        <Route path="/course/" exact component={CourseCreateUpdate} />
    </div>
</div>
)

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <BaseLayout/>
      </BrowserRouter>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);