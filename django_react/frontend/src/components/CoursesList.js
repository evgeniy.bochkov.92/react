import  React, { Component } from  'react';
import  CoursesService  from  './CoursesService';

const  coursesService  =  new  CoursesService();

class  CoursesList  extends  Component {

constructor(props) {
    super(props);
    this.state = {
        courses: []
    };
    this.handleDelete = this.handleDelete.bind(this);
}

componentDidMount() {
    let self = this;
    coursesService.getCourses().then(function (result) {
        console.log(result);
        self.setState({ courses: result })
    });
}
handleDelete(e,pk){
    var self = this;
    coursesService.deleteCourse({pk : pk}).then(()=>{
        var newArr = self.state.courses.filter(function(obj) {
            return obj.pk !== pk;
        });

        self.setState({courses:  newArr})
    });
}
render() {

    return (
        <div  className="courses--list">
            <table className="table">
            <thead key="thead">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            {this.state.courses.map( c  =>
                <tr key={c.pk}>
                <td>{c.pk}  </td>
                <td>{c.name}</td>
                <td>{c.description}</td>
                <td>
                <a  href={"/course/" + c.pk}> Update</a>
                <button  onClick={(e)=>  this.handleDelete(e,c.pk) }> Delete</button>
                </td>
            </tr>)}
            </tbody>
            </table>
        </div>
        );
  }
}
export default CoursesList;