import React, { Component } from 'react';
import CoursesService from './CoursesService';

const coursesService = new CoursesService();

class CourseCreateUpdate extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
      }

      componentDidMount(){
        const { match: { params } } = this.props;
        if(params && params.pk)
        {
          coursesService.getCourse(params.pk).then((c)=>{
            this.refs.name.value = c.name;
            this.refs.description.value = c.description;
          })
        }
      }

      handleCreate(){
        coursesService.createCourse(
          {
            "name": this.refs.name.value,
            "description": this.refs.description.value
        }
        ).then((result)=>{
          alert("Course created!");
        }).catch(()=>{
          alert('There was an error! Please re-check your form.');
        });
      }
      handleUpdate(pk){
        coursesService.updateCourse(
          {
            "pk": pk,
            "name": this.refs.name.value,
            "description": this.refs.description.value
        }
        ).then((result)=>{
          console.log(result);
          alert("Course updated!");
        }).catch(()=>{
          alert('There was an error! Please re-check your form.');
        });
      }
      handleSubmit(event) {
        const { match: { params } } = this.props;

        if(params && params.pk){
          this.handleUpdate(params.pk);
        }
        else
        {
          this.handleCreate();
        }

        event.preventDefault();
      }

      render() {
        return (
          <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>
              Name:</label>
              <input className="form-control" type="text" ref='name' />

            <label>
              Description:</label>
              <textarea className="form-control" ref='description' ></textarea>


            <input className="btn btn-primary" type="submit" value="Submit" />
            </div>
          </form>
        );
      }
}

export default CourseCreateUpdate;